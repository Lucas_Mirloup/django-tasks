"""
GestionTaches URL Configuration
"""
from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.list_task, name="home"),
    path('new_task', views.new_task, name="new_task"),
    path('new_user', views.new_user, name="new_user"),
    path('edit_task/<id_>', views.edit_task, name="edit_task"),
    path('delete_task/<id_>', views.delete_task, name="delete_task"),
    path('edit_user/<id_>', views.edit_user, name="edit_user"),
    path('delete_user/<id_>', views.delete_user, name="delete_user"),
    path('view_task/<id_>', views.view_task, name="view_task"),
    path('list_task', views.list_task, name="task_list"),
    path('list_user', views.list_user, name="user_list"),
]
