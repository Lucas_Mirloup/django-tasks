from django import forms
from .models import Task, User


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ("name", "user", "description", "end_date", "is_finished")
        widgets = {
            'end_date': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d')
        }


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("name", "email")
