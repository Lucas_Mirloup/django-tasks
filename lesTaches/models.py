from django.db import models


class User(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=20)
    email = models.EmailField(max_length=100, null=True)

    def __str__(self):
        return self.name


class Task(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="users", default=True)
    name = models.CharField(max_length=20)
    description = models.TextField()
    creation_date = models.DateField(auto_now_add=True)
    end_date = models.DateField()
    is_finished = models.BooleanField(default=False)

    def __str__(self):
        return self.name + " " + self.description
