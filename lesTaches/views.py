from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse
from django.http import HttpResponseRedirect
from .models import Task, User
from .forms import TaskForm, UserForm


def home(request):
    tasks = Task.objects.all().order_by('-creation_date')
    users = User.objects.all()
    sum_taches = len(tasks)
    sum_users = len(users)
    return render(request, 'lesTaches/home.html',
                  {"tasks": tasks.filter(is_closed=False), "totalTaches": sum_taches, "users": users,
                   "totalUtilisateurs": sum_users})


def list_task(request):
    tasks = Task.objects.all().order_by('-creation_date')
    return render(request, 'lesTaches/taskList.html', {"tasks": tasks})


def list_user(request):
    users = User.objects.all().order_by('name')
    return render(request, 'lesTaches/userList.html', {"users": users})


def view_task(request, id_):
    task = Task.objects.get(id=id_)
    return render(request, 'lesTaches/task.html', {"task": task})


def new_task(request):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('task_list'))
    else:
        users = User.objects.all().order_by('name')
        if len(users) == 0:
            return HttpResponseRedirect(reverse('new_user'))
        form = TaskForm()
        return render(request, 'lesTaches/forms/task.html', {"form": form})


def new_user(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('user_list'))
    else:
        form = UserForm()
        return render(request, 'lesTaches/forms/user.html', {"form": form})


def edit_task(request, id_):
    task = Task.objects.get(id=id_)
    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('task_list'))
    else:
        form = TaskForm(instance=task)
        return render(request, 'lesTaches/forms/task.html', {"form": form})


def edit_user(request, id_):
    user = User.objects.get(id=id_)
    if request.method == 'POST':
        form = UserForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('user_list'))
    else:
        form = UserForm(instance=user)
        return render(request, 'lesTaches/forms/user.html', {"form": form})


def delete_task(request, id_):
    task = Task.objects.filter(id=id_)
    task.delete()
    return HttpResponseRedirect(reverse('task_list'))


def delete_user(request, id_):
    user = User.objects.get(id=id_)
    user.delete()
    return HttpResponseRedirect(reverse('user_list'))
